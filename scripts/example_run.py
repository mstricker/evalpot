#!/usr/bin/env python3
"""
Example on how the functions in this library are used.
"""

import evalpot.evaluator as ep
from evalpot.potential import Potential

import os
import evalpot.helpers as evh
potential_directory = evh.get_potentials_dir()+"/"

library_file = "Wu_2015_Mg_library.meam"
meam_file = "Wu_2015_Mg.meam"
elements = ['Mg']

style = "meam"
pair_coeff = "* * " + potential_directory + library_file + " " + elements[0] + " "\
    + potential_directory + meam_file + " " + elements[0]

atom_types = {"Mg": 1}

potential = Potential(elements, style, pair_coeff)


print(potential.get_elements())
print(potential.get_atom_types())
print(potential.get_pair_style())
print(potential.get_pair_coeff())

# Calculate lattice parameters and cohesive energy

a, covera, energy_cohesive = ep.get_lattice_covera_e_cohesive(potential)

potential.set_material_parameter('lattice constant', a, 'Ang')
potential.set_material_parameter('c/a', covera, '-')
potential.set_material_parameter('cohesive energy', energy_cohesive, 'eV')


print(potential.get_material_parameter('lattice constant'))
print(potential.get_material_parameter('c/a'))
print(potential.get_material_parameter('cohesive energy'))

# Calculate elastic constants

Cij = ep.get_elastic_constants(potential)
C11 = float(Cij[0])
C12 = float(Cij[2])
C13 = float(Cij[3])
C33 = float(Cij[1])
C44 = float(Cij[4])
print('Cij', Cij)

elastic_tensor = {'C11': C11, 'C12': C12, 'C13': C13, 'C33': C33, 'C44': C44}

for key in elastic_tensor:
    potential.set_material_parameter(key, elastic_tensor[key], 'GPa')

elastic_tensor = ['C11', 'C12', 'C13', 'C33', 'C44']

for key in elastic_tensor:
    print(key, potential.get_material_parameter(key))

# Calculate surface energies

# hcp (HKIL) to (HKL), redundancy: I = -(H + K), small caps 'm' refers to
# 'minus' direction
# http://www.uobabylon.edu.iq/eprints/publication_12_9345_1037.pdf for different
# axis definitions

surface_normals = {'basal (0001)': (0, 0, 1),  # basal
                   'prism 1 (10m10)': (1, 0, 0),  # prism 1
                   'prism 2 (11m20)': (1, 1, 0),  # prism 2
                   'pyramidal 1 (1) (1m101)': (1, 0, 1),  # pyramidal 1 (1)
                   'pyramidal 1 (2) (10m12)': (1, 0, 2),  # pyramidal 1 (2)
                   'pyramidal 2 (1) (11m21)': (1, 1, 1),  # pyramidal 2 (1)
                   'pyramidal 2 (2) (11m22)': (1, 1, 2),  # pyramidal 2 (2)
                   'twinning? (10m12)': (1, 0, 2)}  # don't know what that is


print("Surface energies (mJ/m^2")
for normal in surface_normals:
    direction = surface_normals[normal]
    energy = ep.get_surface_energy(potential, direction)
    potential.set_material_parameter('surface energy ' + normal, energy,
                                     'mJ/m^2')
    print(normal, potential.get_material_parameter('surface energy ' +
                                                   str(normal)))

# Calculate stable stacking fault energies, relaxed

sf_planes = {"basal I2": "basal_i2",
             "pyramidal 1 SF2": "pyramidal1_sf2",
             "pyramidal 2 SF1": "pyramidal2_sf1"}

print("Relaxed stacking fault energies (mJ/m^2)")
for key in sf_planes:
    plane = sf_planes[key]
    energy = ep.get_stacking_fault_energy_relaxed(potential, plane, write=True)
    storage_key = "relaxed stacking fault energy " + key
    print('storage_key', storage_key)
    potential.set_material_parameter(storage_key, energy, 'mJ/m^2')
    print(storage_key, potential.get_material_parameter(storage_key))

gsfe = ep.get_generalized_stacking_fault_energy_curve(
    potential, 'basal', write=True)

potential.set_material_parameter('gsfe basal', gsfe, 'mJ/m^2')

gsfe = ep.get_generalized_stacking_fault_energy_curve(potential, 'pyramidal 1')

potential.set_material_parameter('gsfe pyramidal 1', gsfe, 'mJ/m^2')

gsfe = ep.get_generalized_stacking_fault_energy_curve(potential, 'pyramidal 2')

potential.set_material_parameter('gsfe pyramidal 2', gsfe, 'mJ/m^2')

gsfe = ep.get_generalized_stacking_fault_energy_curve(potential, 'prism 1')

potential.set_material_parameter('gsfe prism 1', gsfe, 'mJ/m^2')

e_a = ep.get_energy_a_curve(potential)
potential.set_material_parameter("Energy-lattice", e_a, "AA, eV")

e_v = ep.get_energy_volume_curve(potential)
potential.set_material_parameter("Energy-volume", e_v, "AA^3, eV")


potential.to_json('ref_Mg_meam.json')
