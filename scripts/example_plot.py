#!/usr/bin/env python3
"""Example to outline the future plotting capabilities of `evalpot`"""

from evalpot.potential import Potential

import matplotlib.pyplot as plt

import numpy as np

#potential = Potential.from_json('../tests/ref_Wu2015Mg.json')

potential = Potential.from_json("ref_Mg_meam.json")

potential_name = potential.potential_name

###############################
# Plot elastic constants
###############################
keys = ['C11', 'C12', 'C13', 'C33', 'C44']

vals = []

for key in keys:
    vals.append(potential.get_material_parameter(key)[0])

print(vals)

x = range(len(vals))

fig, ax = plt.subplots(1, 1)
ax.plot(x, vals, 'ko-')
ax.set_xticks(x)
ax.set_xticklabels(keys)
ax.set_ylim([0., max(vals)*1.2])
ax.set_ylabel('Elastic constants (GPa)')
ax.set_title('Potential: ' + potential_name)

plt.show()
plt.close()

###############################
# Plot relaxed stacking fault energies
###############################

# basal
keys = ["relaxed stacking fault energy basal I2",
        "relaxed stacking fault energy pyramidal 1 SF2",
        "relaxed stacking fault energy pyramidal 2 SF1"]

vals = []

for key in keys:
    vals.append(potential.get_material_parameter(key)[0])

x = range(len(vals))
keys_plot = [s.replace("relaxed stacking fault energy ", "") for s in keys]

fig, ax = plt.subplots(1, 1)
ax.plot(x, vals, 'ko')
ax.set_xticks(x)
ax.set_xticklabels(keys_plot)
ax.set_ylim([0., max(vals)*1.2])
ax.set_ylabel(r'Relaxed stacking fault energies (mJ/m$^2$)')
ax.set_title('Potential: ' + potential_name)

plt.show()
plt.close()

###############################
# Plot GSFE curves
###############################

# basal
keys = ["gsfe basal"]

vals = np.asarray(potential.get_material_parameter(keys[0])[0])
refval = vals[0, 1]
vals[:, 1] -= refval
print(vals)

fig, ax = plt.subplots(1, 1)
ax.plot(vals[:, 0], vals[:, 1], 'ko-')

ax.set_xlim([0., 1.])
ax.set_ylim([0., max(vals[:, 1])*1.2])

ax.set_ylabel(r'Along fault vector')
ax.set_ylabel(r'Stacking fault fault energy (mJ/m$^2$)')
ax.set_title('Potential: ' + potential_name + " " + keys[0])

plt.show()
plt.close()

# pyramidal1
keys = ["gsfe pyramidal 1"]

vals = np.asarray(potential.get_material_parameter(keys[0])[0])
refval = vals[0, 1]
vals[:, 1] -= refval
print(vals)

fig, ax = plt.subplots(1, 1)
ax.plot(vals[:, 0], vals[:, 1], 'ko-')

ax.set_xlim([0., 1.])
ax.set_ylim([0., max(vals[:, 1])*1.2])

ax.set_ylabel(r'Along fault vector')
ax.set_ylabel(r'Stacking fault fault energy (mJ/m$^2$)')
ax.set_title('Potential: ' + potential_name + " " + keys[0])

plt.show()
plt.close()

# pyramidal2
keys = ["gsfe pyramidal 2"]

vals = np.asarray(potential.get_material_parameter(keys[0])[0])
refval = vals[0, 1]
vals[:, 1] -= refval
print(vals)

fig, ax = plt.subplots(1, 1)
ax.plot(vals[:, 0], vals[:, 1], 'ko-')

ax.set_xlim([0., 1.])
ax.set_ylim([0., max(vals[:, 1])*1.2])

ax.set_ylabel(r'Along fault vector')
ax.set_ylabel(r'Stacking fault fault energy (mJ/m$^2$)')
ax.set_title('Potential: ' + potential_name + " " + keys[0])

plt.show()
plt.close()

# prism1
keys = ["gsfe prism 1"]

vals = np.asarray(potential.get_material_parameter(keys[0])[0])
refval = vals[0, 1]
vals[:, 1] -= refval
print(vals)

fig, ax = plt.subplots(1, 1)
ax.plot(vals[:, 0], vals[:, 1], 'ko-')

ax.set_xlim([0., 1.])
ax.set_ylim([0., max(vals[:, 1])*1.2])

ax.set_ylabel(r'Along fault vector')
ax.set_ylabel(r'Stacking fault fault energy (mJ/m$^2$)')
ax.set_title('Potential: ' + potential_name + " " + keys[0])

plt.show()
plt.close()
