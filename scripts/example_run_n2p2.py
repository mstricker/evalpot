#!/usr/bin/env python3
"""
Example on how the functions in this library are used.
"""

import evalpot.evaluator as ep
from evalpot.potential import Potential
import os

import evalpot.helpers as evh
#potential_directory = os.path.join(evh.get_potentials_dir(),"n2p2_c20")

potential_directory = "/work1/data/projects/BPNN_pure_Mg/2019_MEAM_data_training/config8/"

elements = ['Mg']
style = 'nnp dir ' + potential_directory + \
    ' showew yes showewsum 10 resetew yes maxew 100000 cflength 1.8897261328 cfenergy 0.0367493254'

cutoff = 11.0
pair_coeff = '* * ' + str(cutoff)

potential = Potential(elements, style, pair_coeff)

# Calculate lattice parameters and cohesive energy

a, covera, energy_cohesive = ep.get_lattice_covera_e_cohesive(potential)

potential.set_material_parameter('lattice constant', a, 'Ang')
potential.set_material_parameter('c/a', covera, '-')
potential.set_material_parameter('cohesive energy', energy_cohesive, 'eV')


print(potential.get_material_parameter('lattice constant'))
print(potential.get_material_parameter('c/a'))
print(potential.get_material_parameter('cohesive energy'))

# Calculate elastic constants

# import ase.build
# Cij = ep.get_elastic_constants_mp(potential, ase.build.bulk('Mg'), use_symmetry=False)
# print("New method:")
# print(Cij)

# #TODO: renable all of this
# print("OLD METHOD:")
# Cij = ep.get_elastic_constants(potential)
# print(Cij)
# for key in Cij:
#    potential.set_material_parameter(key, Cij[key], 'GPa')

# elastic_tensor = ['C11', 'C12', 'C13', 'C33', 'C44']

# for key in elastic_tensor:
#    print(key, potential.get_material_parameter(key))

# Calculate elastic constants

Cij = ep.get_elastic_constants(potential)
C11 = float(Cij[0])
C12 = float(Cij[2])
C13 = float(Cij[3])
C33 = float(Cij[1])
C44 = float(Cij[4])

elastic_tensor = {'C11': C11, 'C12': C12, 'C13': C13, 'C33': C33, 'C44': C44}

for key in elastic_tensor:
    potential.set_material_parameter(key, elastic_tensor[key], 'GPa')

elastic_tensor = ['C11', 'C12', 'C13', 'C33', 'C44']

for key in elastic_tensor:
    print(key, potential.get_material_parameter(key))

# Calculate surface energies

# hcp (HKIL) to (HKL), redundancy: I = -(H + K), small caps 'm' refers to
# 'minus' direction
surface_normals = {'(0001)': (0, 0, 1),
                   '(10m10)': (1, 0, 0),
                   '(11m20)': (1, 1, 0),
                   '(1m101)': (1, 0, 1),
                   '(11m22)': (1, 1, 2),
                   '(10m12)': (1, 0, 2)}

print("Surface energies (mJ/m^2")
for normal in surface_normals:
    direction = surface_normals[normal]
    energy = ep.get_surface_energy(potential, direction)
    potential.set_material_parameter('surface energy ' + normal, energy,
                                     'mJ/m^2')
    print(normal, potential.get_material_parameter('surface energy ' +
                                                   str(normal)))

# Calculate stable stacking fault energies, relaxed

sf_planes = {"basal I2": "basal_i2",
             "pyramidal I SF2": "pyramidal1_sf2",
             "pyramidal II SF1": "pyramidal2_sf1"}

print("Relaxed stacking fault energies (mJ/m^2)")
for key in sf_planes:
    plane = sf_planes[key]
    energy = ep.get_stacking_fault_energy_relaxed(potential, plane)
    potential.set_material_parameter(key, energy, 'mJ/m^2')
    print(key, potential.get_material_parameter(key))


# Energy-volume and energy-lattice constant curves for hcp
e_a = ep.get_energy_a_curve(potential)
potential.set_material_parameter("Energy-lattice", e_a, "AA, eV")

print("Done a_a")

e_v = ep.get_energy_volume_curve(potential)
print("e-v", e_v)
potential.set_material_parameter("Energy-volume", e_v, "AA^3, eV")

# potential.print_all()
potential.to_json('test_Mg_n2p2.json')

print("Done")
