import pytest
from evalpot.potential import Potential
import evalpot.helpers as evh


@pytest.fixture(scope="module")
def get_test_potential_AlSiMgCuFe():
    """Initializes a the Jelinek 2012 potential for testing purposes.

    """

    potential_directory = evh.get_potentials_dir()+"/"
    library_file = "AlSiMgCuFe_library.meam"
    meam_file = "AlSiMgCuFe.meam"
    elements = ["Mg", "Al", "Si", "Fe", "Cu"]
    style = "meam"
    pair_coeff = "* * " + potential_directory + library_file + " " + \
        " ".join(elements) + " " + potential_directory + meam_file + " " +\
        " ".join(elements)
    atom_types = {"Mg": 1, "Al": 2, "Si": 3, "Fe": 4, "Cu": 5}
    potential = Potential(elements, style, pair_coeff,  potential_name="jmeam")
    return potential

@pytest.fixture(scope="module")
def get_test_potential_MEAM_LJ_hybrid():
    """
    Initializes a the Jelinek 2012 with LJ hybrid for testing purposes.
    """
    import os

    atom_types = {"Mg": 1, "Al": 2, "Si": 3, "Fe": 4, "Cu": 5}

    potential_directory = evh.get_potentials_dir()+"/"
    library_file = "AlSiMgCuFe_library.meam"
    library_path = os.path.join(potential_directory, library_file)
    meam_file = "AlSiMgCuFe.meam"
    meam_filepath = os.path.join(potential_directory, meam_file)
    elements = ["Mg", "Al", "Si", "Fe", "Cu"]
    meam_pair_coeff = "pair_coeff * * meam " + library_path + " " + \
                      " ".join(elements) + " " +meam_filepath + " " +\
                      " ".join(elements)

    explicit_lmpcmds = ['pair_style hybrid/overlay lj/cut 5 meam',
                         'pair_coeff * * lj/cut 1 5 5',
                         'pair_modify shift yes',
                         meam_pair_coeff
                           ]

    potential = Potential(elements, "", "",
                explicit_lmpcmds=explicit_lmpcmds,
                potential_name="jmeam_lj_hybrid")
    return potential


@pytest.fixture(scope="module")
def get_test_potential_Wu2015Mg():
    """Initializes the Wu 2015 MSMSE Magnesium MEAM potential"""

    potential_directory = evh.get_potentials_dir() + "/"
    elements = ["Mg"]
    library_file = "Wu_2015_Mg_library.meam"
    meam_file = "Wu_2015_Mg.meam"
    style = "meam"

    pair_coeff = "* * " + potential_directory + library_file + " " +\
        elements[0] + " " + potential_directory + meam_file + " " + elements[0]

    atom_types = {"Mg": 1}

    potential = Potential(elements, style, pair_coeff,
                          potential_name="Wu2015Mg")

    return potential
