import os
import pytest

import ase.build
from tests import regressiontest
import evalpot.evaluator as ep
import evalpot.helpers as evh
from evalpot.potential import Potential
from evalpot.evaluator import (compute_gsfe,
                               generate_surfacelayer,
                               reduce_surfacelayer,
                               cycle_surfacelayer)

from . import conftest


def _get_thetadprime_structure():
    import os
    import ase
    input_path = os.path.join(evh.get_structures_dir(),
                              "POSCAR_Theta_DoublePrime_DFTrelaxed")
    structure = ase.io.read(input_path)
    return structure


@regressiontest
def test_thetadprime_jmeam(get_test_potential_AlSiMgCuFe, tmpdir):
    """Tests the gsfe for theta double prime"""
    potential = get_test_potential_AlSiMgCuFe

    thetaDP = _get_thetadprime_structure()
    thetaDP = generate_surfacelayer(thetaDP,
                                    [1, 1, 0])
    thetaDP = generate_surfacelayer(thetaDP,
                                    [1, 2, 1])

    # reduce function should undo the repeats
    thetaDP = thetaDP.repeat([2, 3, 1])
    thetaDP = reduce_surfacelayer(thetaDP)

    thetaDP = cycle_surfacelayer(thetaDP, 0.25)

    # Just checking a few simple points
    disp_cryst = [(0, 0)]
    disp_cryst += [(x, x+0.1) for x in [0.33, 0.5, 0.66]]

    raw_e, norm_e = ep.compute_gsfe(
        thetaDP,
        potential,
        disp_cryst,
        zlayers=4,
        zgap=0.5,
        relax_distorted_settings={'steps': 20},
    )

    potential.set_material_parameter('GSF_ThetaDP_rawE', raw_e, 'ev/A^2')
    potential.set_material_parameter('GSF_ThetaDP_normE', norm_e, 'ev/A^2')
    potential.set_material_parameter('GSF_ThetaDP_dispcryst', disp_cryst, '-')
    return potential, tmpdir
