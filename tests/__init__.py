import functools
from evalpot.potential import Potential
import numpy as np
import os
import py
from py.path import local


def regressiontest(test):
    """The regressiontest decorator automatically compares the input potential
    against a reference (which is found in this directory with the name
    'ref_POTENTIALNAME.json'). It will then compare the value for each key in
    the computed potential against that of the reference

    """
    @functools.wraps(test)
    def regressiontest_decorator(*args, **kwargs):
        potential, tmpdir = test(*args, **kwargs)
        if not isinstance(potential, Potential):
            raise Exception("Regression test must return a potential as the"
                            " first argument.")
        if not isinstance(tmpdir, local):
            raise Exception("Regression test must return a tmpdir as the second"
                            " argument.")

        # write out the potential to the tmpdir using the test as a name
        outputfile = tmpdir.mkdir(test.__name__).join('potential_dump.json')
        potential.to_json(outputfile)

        # load the reference data
        potential_name = potential.parameters['potential_name']
        refpotential_path = os.path.dirname(__file__) + \
            "/ref_{}.json".format(potential_name)

        ref_potential = Potential.from_json(refpotential_path)

        # compare the contents of the potential against the reference
        for key in potential.parameters:
            print("TESTING: {}".format(key))
            # Some keys will be unique to each computer and should be skipped
            skip_keys = ['pair_coeff', 'explicit_lmpcmds']
            if key in skip_keys:
                print("SKIPPED: {}".format(key))
                continue
            # The initial parameters do not have units, unlike all the material
            # parameters
            if key in potential.initial_parameters:
                val = potential.parameters[key]
                ref_val = ref_potential.parameters[key]
                assert val == ref_val
                print("PASSED: {}".format(key))
                continue

            # pull the value and units out for each of the material parameters
            val = potential.parameters[key][0]
            units = potential.parameters[key][1]
            try:
                ref_val = ref_potential.parameters[key][0]
                ref_units = ref_potential.parameters[key][1]
            except KeyError:
                raise Exception("Reference potential {} does not contain a "
                                "{} ""key.".format(refpotential_path,
                                                   key))

            # First compare with numpy if possible, else check equality directly
            try:
                # rtol : relatice tolerance
                # atol : absolute tolerance
                assert np.allclose(val, ref_val, rtol=1e-5, atol=1e-6)
            except TypeError:
                assert val == ref_val
            # Check the units are the same as well
            assert units == ref_units
            print("PASSED: {}".format(key))

        return

    return regressiontest_decorator
