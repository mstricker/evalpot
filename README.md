# evalpot

A library to automatically evaluate interatomic potentials based on ASE (for
structures), Elastic (elastic constant calculations) and Lammps as a calculator
for energies and relaxations.

`evalpot` is currently focussed on Magnesium properties with the MEAM framework
(plus n2p2 in the near future). As need arises, this will be expanded and made
more flexible.

This library provides functionality for the calculation of
- Lattice parameter
- c/a ratio
- Cohesive energy
- Elastic constants
- Surface energies
- Stable stacking fault energies with the tilted cell method (relaxing all _3j
  components)

## Setup
Inside the library folder run
`python3 setup.py install --u`
to install it for your current user.
And all functionality is readily available as show in the example evaluation.

## Dependencies
The library makes use of the python packages `ase` and `elastic`.

## Mg potentials
The example folder for potentials includes two different MEAM-type potentials
from Wu et al. Citations are given in the respective files.

## Example evaluation
`scripts/example_run.py` gives an example on how to use the functionality.

## Lammps interface
Compile `lammps` as you need it, including python, meam, etc. And don't forget
to make a shared library with `make mpi mode=shlib` for it to be accessible by
ase as a calculator and `make mpi` so that the executable is found.

## Careful note
ASE 3.17.0 is stable, ASE 3.18.0 has an issue with the optimizer

## TODO
- solute-solute test: calculate energy on relaxed cell
