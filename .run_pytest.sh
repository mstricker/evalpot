#WARNING: do not leave --basetemp empty and don't set it to a directory containing important data!
pytest --cov=../evalpot --cov-report html --showlocals --disable-warnings \
       --basetemp ./tests.tmp_data  -q ./tests && RES=0 || RES=1
COVER_PAGE=$(realpath $(find ./htmlcov/ -name index.html))
if [ -z "$COVER_PAGE" ]; then
  echo "ERROR: no coverage report found!"
fi
echo "Testing data stored in $TEST_DIR.tmp_data"
echo "Coverage report stored in $COVER_PAGE"
exit $RES
