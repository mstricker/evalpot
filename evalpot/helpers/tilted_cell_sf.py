#!/usr/bin/env/python3
"""Helper functions for tilted cell method for calculating the stable stacking
fault energy
"""


def get_tilted_cell(atoms, replicas, t_vec):
    """Returns the tilted cell given a reference cell and a vector to shift
    The method is described in Yin et al. (2017) Acta Mater 123, 223-234

    INPUT
    atoms : ASE Atoms object

    replicas : list of integers how often the unit cell was repeated

    t_vec : vector to shift third axis, third component needs to be zero

    OUTPUT
    atoms_tilt : ASE Atoms object with third axis shifted by t_vec
    """

    import numpy as np
    from ase import Atoms

    # atoms_tilt = atoms.copy()
    cell_tilt = atoms.cell

    # vectors to project a1, a2 on
    e1 = np.array([1., 0., 0.])
    e2 = np.array([0., 1., 0.])

    a1 = np.array(cell_tilt[0] / replicas[0])
    a2 = np.array(cell_tilt[1] / replicas[1])

    a1 *= e1
    a2 *= e2

    c = np.array(cell_tilt[2])
    tprime = t_vec[0] * a1 + t_vec[1] * a2
    c += tprime

    atoms.cell[2] = c

    return atoms


def get_tilted_cell_dt(atoms, dt):
    """Returns the tilted cell directly with a given translation vector
    INPUT
    atoms : ASE Atoms object

    t_vec : vector to shift third axis, third component needs to be zero

    OUTPUT atoms_tilt : ASE Atoms object with third axis shifted by t_vec

    """
    import numpy as np
    from ase import Atoms

    e1 = np.array([1., 0., 0.])
    e2 = np.array([0., 1., 0.])

    c = np.array(atoms.cell[2])
    tprime = dt[0] * e1 + dt[1] * e2
    c += tprime

    atoms.cell[2] = c

    return atoms
