"""
Various helper functions for calculations
"""
__all__ = ['get_potentials_dir', 'get_structures_dir',
           'relax_atoms_only', 'relax_cell_only',
           'relax_atoms_cell', 'relax_atoms_direction',
           'relax_atoms_cell_direction']


def get_potentials_dir():
    """
    Returns the (pip-installed) directory that contains the potentials
    """
    import os
    import evalpot
    evalpot_path = os.path.dirname(evalpot.__file__)
    potentials_path = os.path.join(evalpot_path, "potential_data")
    return potentials_path


def get_structures_dir():
    """
    Returns the (pip-installed) directory that contains the potentials
    """
    import os
    import evalpot
    evalpot_path = os.path.dirname(evalpot.__file__)
    potentials_path = os.path.join(evalpot_path, "structure_data")
    return potentials_path


def relax_atoms_only(structure, steps=1000, fmax=1e-6,
                     logfile=None, trajectorypath=None):
    """Keeps the cell fixed and relaxes all the atoms"""

    from ase.optimize import BFGS

    dyn = BFGS(structure, logfile=logfile, trajectory=trajectorypath)
    dyn.run(steps=steps, fmax=fmax)

    return dyn.atoms


def relax_cell_only(structure, strain_mask=[1, 1, 1, 1, 1, 1], steps=1000,
                    fmax=1e-6, logfile=None, trajectorypath=None):
    """Relaxes the cell but keeping the scaled atomic positions"""

    from ase.optimize import BFGS
    from ase.constraints import StrainFilter

    sf = StrainFilter(structure, mask=strain_mask)
    dyn = BFGS(sf, logfile=logfile, trajectory=trajectorypath)
    dyn.run(steps=steps, fmax=fmax)  # TODO: possibly pipe to logfile?

    return dyn.atoms.atoms


def relax_atoms_cell(structure, strain_mask=[1, 1, 1, 1, 1, 1], steps=1000,
                     fmax=1e-6, logfile=None, trajectorypath=None):
    """Relaxes the cell and atomic positions"""

    from ase.optimize import BFGS
    from ase.constraints import UnitCellFilter

    ucf = UnitCellFilter(structure, mask=strain_mask)
    dyn = BFGS(ucf, logfile=logfile, trajectory=trajectorypath)
    dyn.run(steps=steps, fmax=fmax)  # TODO: pipe to logfile?

    return dyn.atoms.atoms


def relax_atoms_direction(structure, atom_direction=[1, 1, 1],
                          steps=1000, fmax=1e-6,
                          logfile=None, trajectorypath=None):
    """Relaxes (only) atomic positions with an additional directional constraint
    on the positions. This module is purely for backwards-compatability with
    prior work

    """
    from ase.constraints import FixedLine
    from ase.optimize import BFGS
    constraints = [FixedLine(i, atom_direction) for i in range(len(structure))]
    structure.set_constraint(constraints)
    dyn = BFGS(structure,  logfile=logfile, trajectory=trajectorypath)
    dyn.run(steps=steps, fmax=fmax)
    return dyn.atoms


def relax_atoms_cell_direction(structure, strain_mask=[1, 1, 1, 1, 1, 1],
                               atom_direction=[1, 1, 1],
                               steps=1000, fmax=1e-6,
                               logfile=None, trajectorypath=None):
    """Relaxes cell and atomic positions with an additional directional constraint
    on the positions. This is for example needed in generalized stacking fault
    energy curves where atoms are only allowed to relax out of plane.

    """
    from ase.constraints import UnitCellFilter, FixedLine
    from ase.optimize import BFGS

    constraints = [FixedLine(i, atom_direction) for i in range(len(structure))]
    structure.set_constraint(constraints)
    ucf = UnitCellFilter(structure, mask=strain_mask)

    dyn = BFGS(ucf, logfile=logfile, trajectory=trajectorypath)
    dyn.run(fmax=fmax, steps=steps)

    return dyn.atoms.atoms
