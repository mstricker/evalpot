#!/usr/bin/env/python3
"""Helper functions to get reference cells for crystallographic directions.

"""


def get_basal_unit_cell(element, a, c):
    """Constructs a minimal basal unit cell in hcp"""

    import numpy as np
    from ase import Atoms

    v1 = [a, 0., 0.]
    v2 = [-0.5 * a, np.sqrt(3./2.) * a, 0.]
    v3 = [0., 0., c]
    cell_vec = [v1, v2, v3]

    p1 = [0., 0., 0.]
    p2 = [2./3., 1./3., 0.5]
    positions = [p1, p2]

    atom_elements = element + str(len(positions))

    atoms = Atoms(atom_elements, scaled_positions=positions, cell=cell_vec,
                  pbc=[1, 1, 1])

    return atoms


def get_basal_unit_cell_dft(element, a, c):
    """Constructs a minimal basal unit cell in hcp based on DFT data from B. Yin
    which was used to train a neural network potential. x-y plane is the
    stacking fault plane, z direction is orthogonal.

    """

    from ase import Atoms

    v1 = [1. * a, 0., 0.]
    v2 = [-0.5 * a, 0.866025403784439 * a, 0.0 * a]
    v3 = [0., 0., 16.2714889018677447 * a / 10.]
    cell_vec = [v1, v2, v3]

    p1 = [0., 0., 0.]
    p2 = [0.3333333333333357, 0.6666666666666643, 0.0499999762324776]
    positions = [p1, p2]

    atom_elements = element + str(len(positions))

    atoms = Atoms(atom_elements, scaled_positions=positions, cell=cell_vec,
                  pbc=[1, 1, 1])

    return atoms


def get_pyramidal1_unit_cell(element, a, c):
    """Constructs a minimal pyramidal 1 unit cell in hcp"""

    import numpy as np
    from ase import Atoms

    l = np.sqrt(a**2 + c**2)
    e2len = np.sqrt(c**2 + 0.75 * (a**2))
    gamma = np.arctan(np.sqrt(3.)/2.*a/c)
    d = c * np.sin(gamma)
    e = c * np.cos(gamma)
    beta = np.arcsin(a/2./l)
    f = 0.5 * e2len * np.tan(beta)/a

    v1 = [a, 0., 0.]
    v2 = [-0.5 * a, e2len, 0.]
    v3 = [0., e, d]
    cell_vec = [v1, v2, v3]

    p1 = [0., 0., 0.]
    p2 = [f + 0.5, 0.5, 0.]
    positions = [p1, p2]

    atom_elements = element + str(len(positions))
    atoms = Atoms(atom_elements, scaled_positions=positions, cell=cell_vec,
                  pbc=[1, 1, 1])

    return atoms


def get_pyramidal1_unit_cell_dft(element, a, c):
    """Constructs a minimal pyramidal 2 unit cell in hcp based on DFT data from
    B. Yin which was used to train a neural network potential. x-y plane is the
    stacking fault plane, z direction is orthogonal.

    """

    from ase import Atoms

    v1 = [1. * a, 0., 0.]
    v2 = [-0.5 * a, 1.8433297195770479 * a, 0.]
    v3 = [4.9999999999999982, -4.0687240705482006, 7.6473429198036973]
    v3 = [v * a / 10. for v in v3]
    cell_vec = [v1, v2, v3]

    p1 = [0.0000022225883924, 0.9999987409164461, 0.9999994295739612]
    p1 = [0., 0., 0.]
    p2 = [0.6666644780412696,  0.5000012398437121,  0.0166672283761145]
    positions = [p1, p2]

    atom_elements = element + str(len(positions))
    atoms = Atoms(atom_elements, scaled_positions=positions, cell=cell_vec,
                  pbc=[1, 1, 1])

    return atoms


def get_pyramidal2_unit_cell(element, a, c):
    """Constructs a minimal pyramidal 2 unit cell in hcp"""

    import numpy as np
    from ase import Atoms

    covera = c/a
    l = np.sqrt(a**2 + c**2)
    theta = np.arctan(1./covera)

    v1 = [a * np.sqrt(3.), 0., 0.]
    v2 = [0., l, 0.]
    v3 = [0., -np.sin(theta) * a, np.cos(theta) * a]
    cell_vec = [v1, v2, v3]

    p1 = [0., 0., 0.]
    p2 = [0.5, 0., 0.5]
    p3 = [5./6., 0.5, 0.]
    p4 = [2./6., 0.5, 0.5]
    positions = [p1, p2, p3, p4]

    atom_elements = element + str(len(positions))
    atoms = Atoms(atom_elements, scaled_positions=positions, cell=cell_vec,
                  pbc=[1, 1, 1])

    return atoms


def get_pyramidal2_unit_cell_dft(element, a, c):
    """Constructs a minimal pyramidal 2 unit cell in hcp based on DFT data from
    B. Yin which was used to train a neural network potential. x-y plane is the
    stacking fault plane, z direction is orthogonal.

    """

    from ase import Atoms

    v1 = [1.7320508075688770 * a,    0.,    0.]
    v2 = [0., 1.9099383380297910 * a, 0.]
    v3 = [0., -5.2357711245880134 * a / 10, 8.5210207544552787 * a / 10]
    cell_vec = [v1, v2, v3]

    p1 = [0., 0., 0.0]
    p2 = [0.5,  0.,  0.05]
    p3 = [0.8333333333333357,  0.5000000195027425,  0.]
    p4 = [0.3333333333333357,  0.5000000027330671,  0.05]
    positions = [p1, p2, p3, p4]

    atom_elements = element + str(len(positions))
    atoms = Atoms(atom_elements, scaled_positions=positions, cell=cell_vec,
                  pbc=[1, 1, 1])

    return atoms


def get_prism1_unit_cell(element, a, c):
    """Constructs a minimal prism 1 unit cell in hcp. x-y is the stacking fault
    plane, z-direction i orthogonal

    """

    from ase import Atoms
    import numpy as np

    h = np.sqrt(3.)/2. * a
    r = 1/(2 * np.sqrt(3.)) * a

    v1 = [a, 0., 0.]
    v2 = [0., c, 0.]
    v3 = [0., 0., 2*h]
    cell_vec = [v1, v2, v3]

    p1 = [0., 0., 0.]
    p2 = [a/2., 0., h]
    p3 = [0., c/2., h-r]
    p4 = [a/2., c/2., 2*h - r]
    positions = [p1, p2, p3, p4]

    atom_elements = element + str(len(positions))
    atoms = Atoms(atom_elements, scaled_positions=positions, cell=cell_vec,
                  pbc=[1, 1, 1])

    return atoms
