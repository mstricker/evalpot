#!/usr/bin/env/python3
"""
Various helper functions to de-globber the main routines.
"""


def run_different_tvec(atoms, replicas, tvec, parameters, energy_0):
    """Experimental range calculator for stacking fault energies
    """
    import numpy as np
    from ase.calculators.lammpsrun import LAMMPS
    from ase.optimize import BFGS
    from ase.constraints import UnitCellFilter

    fmax = 1e-6
    steps = 500
    maxstep = 1e-2

    max2 = tvec[1]
    min2 = max2/2.
    dstep = (max2 - min2)/10.
    trange = np.arange(min2, max2, dstep)
    #print('trange', trange)

    lammps = LAMMPS(parameters=parameters)

    logfile = 'log.tilted_cell'

    for t2 in trange:
        t = [0., t2]
        atoms_tilt = get_tilted_cell(atoms, replicas, t)
        atoms_tilt.set_calculator(lammps)
        atoms_tilt_rlx = UnitCellFilter(atoms_tilt, mask=[0, 0, 1, 1, 1, 0])
        dyn_rlx = BFGS(atoms_tilt_rlx, logfile=logfile, maxstep=maxstep)
        dyn_rlx.run(fmax=fmax, steps=steps)
        atoms_tilt_rlx = dyn_rlx.atoms.atoms
        e_ref_tilt = atoms_tilt_rlx.get_potential_energy()
        dE = e_ref_tilt - energy_0
        area = get_cell_area(atoms_tilt_rlx, [0, 1])

        ev_to_j = 1.6021766e-19
        a_to_m = 1e-10
        mult = ev_to_j / a_to_m / a_to_m * 1000
        print('dE for t', t, dE/area * mult)

    return True
