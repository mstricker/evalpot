#!/usr/bin/env python3
"""
Warning: this script is a direct import from a jupyter notebook. Some of the functions
will be redundant with other evaluators, to be consolidated later
"""

__all__ = ["get_solsol_binding"]

import pandas as pd
import numpy as np

from evalpot.helpers import relax_atoms_only, relax_cell_only


# TODO: move this to a more generic module
def return_nn_distanceAndIndex(ase_supercell):
    supercell_frame = pd.DataFrame(ase_supercell.get_positions(),
                                   columns=['x', 'y', 'z'])
    supercell_frame['distance'] = supercell_frame.apply(
        np.linalg.norm, axis=1)
    supercell_frame['distance'] = supercell_frame['distance'].round(9)

    supercell_shape = ase_supercell.get_cell()
    lx = supercell_shape[0][0]
    ly = supercell_shape[1][1]
    lz = supercell_shape[2][2]

    # we assume the first solute is always at the origin
    tol = 0.1
    supercell_frame = supercell_frame[(supercell_frame['x'] <= lx / 2. + tol) &
                                      (supercell_frame['y'] <= ly / 2. + tol) &
                                      (supercell_frame['z'] <= lz / 2. + tol)]

    supercell_frame.sort_values('distance', inplace=True)
    supercell_frame.drop_duplicates(subset='distance', inplace=True)

    distance_values = supercell_frame['distance'].values
    index_values = supercell_frame.index.values

    nn_distanceindex_frame = pd.DataFrame(data=zip(distance_values, index_values),
                                          columns=['distances', 'indexes'])

    return nn_distanceindex_frame


def get_solsol_binding(potential, pure_structure,
                       sol1_element, sol2_element,
                       max_index=None,
                       atom_relax_settings={}):

    pure_structure = potential.set_calculator(pure_structure)
    nn_distanceindex_frame = return_nn_distanceAndIndex(pure_structure)
    purematrix_E = pure_structure.get_potential_energy()
    singlesol1_structure = pure_structure.copy()

    # Vacancy can only be produced via deleting atoms which shifts
    # the index of all positions. Vacancies must be handled with great care
    if sol1_element == "Vac":
        del singlesol1_structure[0]
    else:
        singlesol1_structure[0].symbol = sol1_element
    singlesol1_structure = potential.set_calculator(singlesol1_structure)
    relax_atoms_only(singlesol1_structure, **atom_relax_settings)
    singlesol1_E = singlesol1_structure.get_potential_energy()
    if sol1_element == "Vac":
        singlesol1_structure = pure_structure.copy()

    # NOTE: there is probably a way to refactor this with sol1
    if sol1_element != sol2_element:
        singlesol2_structure = pure_structure.copy()
        if sol2_element == "Vac":
            del singlesol2_structure[0]
        else:
            singlesol2_structure[0].symbol = sol2_element
        singlesol2_structure = potential.set_calculator(singlesol2_structure)
        relax_atoms_only(singlesol2_structure, **atom_relax_settings)
        singlesol2_E = singlesol2_structure.get_potential_energy()
    else:
        singlesol2_E = singlesol1_E

    distances = []
    solsol_energies = []
    if max_index is None:
        max_index = len(nn_distanceindex_frame)
    for i in range(1, max_index):
        secondsol_structure = singlesol1_structure.copy()
        secondsol_index = nn_distanceindex_frame['indexes'][i]
        distance = np.linalg.norm(
            secondsol_structure[secondsol_index].position)
        distances.append(distance)

        if sol2_element == "Vac":
            del secondsol_structure[secondsol_index]
        else:
            secondsol_structure[secondsol_index].symbol = sol2_element
        if sol1_element == "Vac":
            del secondsol_structure[0]

        secondsol_structure = potential.set_calculator(secondsol_structure)
        relax_atoms_only(secondsol_structure, **atom_relax_settings)
        solsol_energy = secondsol_structure.get_potential_energy()
        solsol_energies.append(solsol_energy)
    solsol_energies = np.array(solsol_energies)

    binding_E = solsol_energies + purematrix_E - singlesol1_E - singlesol2_E
    binding_E = binding_E * 1000  # meV units

    return distances, binding_E
