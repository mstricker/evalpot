#!/usr/bin/env python3
"""Collection of function specific for a material value (or multiples thereof,
if e.g. different crystallographic planes
"""
__all__ = ["get_lattice_covera_e_cohesive", "get_elastic_constants_mp",
           "get_elastic_constants", "get_surface_energy",
           "get_stacking_fault_energy_relaxed",
           "get_generalized_stacking_fault_energy_curve",
           "get_energy_volume_curve",
           "get_energy_a_curve"]


def get_lattice_covera_e_cohesive(potential, element_index=0):
    """A helper function which takes the definition of a potential as input and
    returns a, c/a and cohesive energy of a bulk Mg structure

    The strain filter is necessary to relax the cell in x/y/z normal directions
    """

    from ase.build import bulk
    from ase.spacegroup import crystal
    from ase.calculators.lammpslib import LAMMPSlib
    from ase.constraints import StrainFilter
    from ase.optimize import BFGS
    from evalpot.helpers import relax_atoms_cell

    # guessing the lattice parameter from experimental values
    a_guess = 3.2
    covera_guess = 1.623  # (-)
    c_guess = covera_guess * a_guess

    element = potential.get_elements()[element_index]

    #atoms = bulk(element, 'hcp', a=a_guess, covera=covera_guess)

    atoms = crystal(element, [(1./3., 2./3., 3./4.)], spacegroup=194,
                    cellpar=[a_guess, a_guess, c_guess, 90, 90, 120])

    atoms = potential.set_calculator(atoms)
    atoms = relax_atoms_cell(atoms)

    cell = atoms.get_cell()
    a = float(cell[0][0])
    c = float(cell[2][2])
    c_over_a = float(c/a)

    natoms = atoms.get_positions().shape[0]
    energy_coh = atoms.get_potential_energy()/natoms

    return a, c_over_a, energy_coh


def get_elastic_constants_mp(potential, structure_ase,
                             use_symmetry=False,
                             relax_distorted_settings=None,
                             strain_amounts=[-0.01, -0.005, 0.005, 0.01]):
    """
    Computation of elastic constants using materials project
    """
    # TODO: use_symmetry falsely sets some values to zero, find out why (AiiDA
    # version seems OK)
    import os
    import numpy as np
    import copy

    import lammps
    import ase
    import ase.build
    import pymatgen as mg
    import pymatgen.analysis.elasticity
    import evalpot.helpers as evh
    from pymatgen.io.ase import AseAtomsAdaptor
    from pymatgen.analysis.elasticity.stress import Stress
    from pymatgen.analysis.elasticity import ElasticTensor
    from pymatgen.core.tensors import symmetry_reduce

    # TODO: use_symmetry falsely sets some values to zero, find out why (AiiDA
    # version seems OK)
    if use_symmetry:
        raise NotImplementedError(
            "use_symmetry is buggy and is disabled for now")

    # collect 'unstrained' stress
    structure_ase = potential.set_calculator(structure_ase)
    eq_stress_voigt = structure_ase.get_stress()
    eq_stress_mg = Stress.from_voigt(eq_stress_voigt)

    # strain_amounts=[0.001]
    structure_mat = AseAtomsAdaptor.get_structure(structure_ase)

    deformed_mat_set = \
        mg.analysis.elasticity.DeformedStructureSet(structure_mat,
                                                    norm_strains=strain_amounts,
                                                    shear_strains=strain_amounts)

    deformations = deformed_mat_set.deformations
    if use_symmetry:
        symmetry_operations_dict = \
            symmetry_reduce(deformed_mat_set.deformations, structure_mat)
        deformations = [x for x in symmetry_operations_dict]

    applied_strains = []
    resultant_stresses = []
    resultant_energy = []
    for i in range(len(deformations)):
        deformation = deformations[i]

        # evaluate the applied strain
        applied_strain = deformation.green_lagrange_strain
        applied_strains.append(applied_strain)

        # setup the structure by applying the strain
        deformed_mat = AseAtomsAdaptor.get_structure(structure_ase)
        # .apply_strain method broken:
        deformed_mat = deformation.apply_to_structure(deformed_mat)
        deformed_ase = AseAtomsAdaptor.get_atoms(deformed_mat)
        deformed_ase = potential.set_calculator(deformed_ase)
        if relax_distorted_settings is not None:
            deformed_ase = evh.relax_atoms_only(deformed_ase,
                                                **relax_distorted_settings)

        # collect stress
        resultant_stress_voigt = deformed_ase.get_stress()
        resultant_stress_mg = Stress.from_voigt(resultant_stress_voigt)
        #resultant_stress_matrix = convert_voigt2matrix(resultant_stress_voigt)
        # Lammps uses (Bar) for pressure
        #resultant_stress_mg = Stress(resultant_stress_matrix)
        resultant_stresses.append(resultant_stress_mg)

        # collect energy
        resultant_energy.append(deformed_ase.get_total_energy())

        # add symmetry equivalent stress & strain
        if use_symmetry:
            symmetry_operations = [
                x for x in symmetry_operations_dict[deformation]]
            for symm_op in symmetry_operations:
                applied_strains.append(applied_strain.transform(symm_op))
                resultant_stresses.append(
                    resultant_stress_mg.transform(symm_op))
    compliance = ElasticTensor.from_independent_strains(strains=applied_strains,
                                                        stresses=resultant_stresses,
                                                        eq_stress=eq_stress_mg)
    compliance = compliance*(ase.units.GPa**(-1))
    return np.array(compliance.voigt_symmetrized.voigt)
    # return applied_strains, resultant_stresses, eq_stress_mg


def get_elastic_constants(potential, element_index=0):
    """adapted from http://wolf.ifj.edu.pl/elastic/lib-usage.html
    #calculation-of-the-elastic-tensor
    """

    from ase.build import bulk
    from ase.calculators.lammpslib import LAMMPSlib
    from ase.constraints import StrainFilter
    from ase.optimize import BFGS
    import ase.units as units

    from elastic import get_elastic_tensor, get_pressure
    from elastic import get_elementary_deformations

    #    from evalpot.helpers import vc_relax
    from evalpot.helpers import relax_cell_only

    element = potential.get_elements()[element_index]
    a = potential.get_material_parameter('lattice constant')[0]
    covera = potential.get_material_parameter('c/a')[0]

    atoms = bulk(element, 'hcp', a=a, covera=covera)

    atoms = potential.set_calculator(atoms)

    logfile = 'log.elastic_constants'

    atoms = relax_cell_only(atoms)

    print("Elastic constant calculation residual pressure: %.3f bar" %
          (get_pressure(atoms.get_stress())))

    # n : number of steps, d : amount of strain
    systems = get_elementary_deformations(atoms, n=1, d=0.01)

    res = []
    for n, system in enumerate(systems):
        system = potential.set_calculator(system)
        val = system.get_potential_energy()
        res.append(system)

    Cij, _ = get_elastic_tensor(atoms, systems=res)

    Cij /= units.GPa

    return Cij


def get_surface_energy(potential, direction):
    """Calculate surface energy defined by direction, return in mJ/m^2"""

    from ase.build import bulk, surface
    from ase.calculators.lammpslib import LAMMPSlib
    from ase.units import eV, kJ, Ang, m

    element = potential.get_elements()[0]
    a = potential.get_material_parameter('lattice constant')[0]
    covera = potential.get_material_parameter('c/a')[0]
    e_cohesive = potential.get_material_parameter('cohesive energy')[0]

    atoms = bulk(element, 'hcp', a=a, covera=covera)
    surf = surface(atoms, direction, 3)
    surf.center(vacuum=20, axis=2)

    surf = potential.set_calculator(surf)

    # surf.set_calculator(lammps)
    pbc = [True, True, True]
    surf.set_pbc(pbc)

    e_pot = surf.get_potential_energy()
    natoms = surf.get_positions().shape[0]
    area = surf.cell[0][0] * surf.cell[1][1]

    energy = (e_pot - natoms * e_cohesive)
    # from eV/A^2 to mJ/m^2 for 2 surfaces
    mult = eV / kJ * 1000 * m * m * 1000 / 2.

    return float(energy * mult / area)


def get_stacking_fault_energy_relaxed(potential, plane, verbose=False,
                                      write=False):
    """Calculates the relaxed stacking fault energy with the tilted cell method

    plane : string with basal_i2, pyramidal1_sf2, pyramidal2_sf1

    t_vec is the shift vector for tilting the cell

    Returns are in (mJ/m^2)

    """

    from evalpot.helpers import tilted_cell_sf, geometry, reference_cells

    from evalpot.helpers import relax_atoms_cell

    import numpy as np
    from ase import Atoms
    #from ase.constraints import StrainFilter, UnitCellFilter
    from ase.calculators.lammpslib import LAMMPSlib
    from ase.optimize import BFGS
    from ase.units import eV, kJ, Ang, m

    reverse_map = {"basal_i2": "basal I2",
                   "pyramidal1_sf2": "pyramidal 1 SF2",
                   "pyramidal2_sf1": "pyramidal 2 SF1"}

    element = potential.get_elements()[0]
    a = potential.get_material_parameter('lattice constant')[0]
    covera = potential.get_material_parameter('c/a')[0]
    c = covera * a

    if (plane == "basal_i2"):
        atoms_base = reference_cells.get_basal_unit_cell(element, a, c)

        replicas = [1, 1, 10]
        atoms = atoms_base.repeat(replicas)

        t_vec = [-0.501, 0.334]

    elif plane == "pyramidal1_sf2":
        atoms_base = reference_cells.get_pyramidal1_unit_cell(element, a, c)
        replicas = [1, 1, 10]
        atoms = atoms_base.repeat(replicas)

        t_vec = [0., 0.403]

    elif plane == "pyramidal2_sf1":
        atoms_base = reference_cells.get_pyramidal2_unit_cell(element, a, c)

        replicas = [1, 1, 10]
        atoms = atoms_base.repeat(replicas)

        t_vec = [0., 0.480]

    else:
        raise NotImplementedError("Not implemented, please choose from 'basal_i2',\
        'pryamidal1_sf2', 'pyramidal2_sf1']")

    # optimizer settings
    fmax = 1e-6  # as per conversation with Daniel
    steps = 2000
    logfile = 'log.tilted_cell'

    if(write):
        atoms_base.write("atoms_ref.xyz")

    atoms = potential.set_calculator(atoms)

    if(verbose):
        print('Relaxing reference cell')

    atoms_rlx = relax_atoms_cell(atoms)

    e_ref = atoms_rlx.get_potential_energy()
    natoms = atoms_rlx.get_positions().shape[0]

    if(write):
        atoms.write("atoms_ref.xyz")
        atoms_rlx.write("atoms_ref_rlx.xyz")
    atoms_tilt = tilted_cell_sf.get_tilted_cell(atoms_rlx, replicas, t_vec)

    if(write):
        atoms_tilt.write("atoms_tilt.xyz")

    atoms_tilt = potential.set_calculator(atoms_tilt)

    if(verbose):
        print('Relaxing tilted cell')

    # allow only \sigma_3j to be non zero
    atoms_tilt_rlx = relax_atoms_cell(
        atoms_tilt, strain_mask=[0, 0, 1, 1, 1, 0])

    e_ref_tilt = atoms_tilt_rlx.get_potential_energy()

    if(write):
        atoms_tilt_rlx.write("atoms_tilt_rlx.xyz")

    dE = e_ref_tilt - e_ref
    area = geometry.get_cell_area(atoms_tilt_rlx, [0, 1])

    mult = eV / kJ * 1000 * m * m * 1000

    return float(dE/area * mult)


def get_generalized_stacking_fault_energy_curve(potential, plane, write=False):
    """Calculated the GSFE curve with a rigid shift of the tilted cell. Relaxation
    only out of plane (classical way):

    """

    from evalpot.helpers import tilted_cell_sf, geometry, reference_cells
    from evalpot.helpers import relax_atoms_cell, relax_atoms_cell_direction

    import numpy as np
    from ase import Atoms
    from ase.constraints import StrainFilter, UnitCellFilter, FixedLine
    from ase.calculators.lammpslib import LAMMPSlib
    from ase.optimize import BFGS
    from ase.units import eV, kJ, Ang, m

    element = potential.get_elements()[0]
    a = potential.get_material_parameter('lattice constant')[0]
    covera = potential.get_material_parameter('c/a')[0]
    c = covera * a

    if plane == 'basal':
        unit_cell = reference_cells.get_basal_unit_cell_dft(element, a, c)

        replicas = [1, 1, 10]
        # unit_cell.wrap()
        atoms = unit_cell.repeat(replicas)

        # Total shift vector from DFT calculations
        t_vec = [-0.6, 0.34641016]

        npoints = 12

    elif plane == 'pyramidal 1':
        unit_cell = reference_cells.get_pyramidal1_unit_cell_dft(element, a, c)

        #unit_cell.cell[2][2] *= 1.03
        replicas = [1, 1, 10]
        atoms = unit_cell.repeat(replicas)

        # Shift vector from DFT calculations
        t_vec = [0., 1.84332972]

        npoints = 20

    elif plane == 'pyramidal 2':
        unit_cell = reference_cells.get_pyramidal2_unit_cell(element, a, c)

        replicas = [1, 1, 10]
        atoms = unit_cell.repeat(replicas)

        # Total shift vector from DFT calculations
        t_vec = [0., 1.90993834]

        npoints = 20

    elif plane == 'prism 1':
        unit_cell = reference_cells.get_prism1_unit_cell(element, a, c)

        replicas = [1, 1, 10]
        atoms = unit_cell.repeat(replicas)

        # Total shift vector for SF1
        t_vec = [1., 0.]

        npoints = 20

    else:
        raise NotImplementedError("Not implemented, please choose from 'basal',\
        'pryamidal 1', 'pyramidal 2', 'prism 1']")

    fmax = 1e-6
    steps = 2000
    logfile = 'log.tilted_cell'

    atoms_rlx = atoms.copy()
    atoms_rlx.wrap()
    atoms_rlx = potential.set_calculator(atoms_rlx)

    atoms_rlx = relax_atoms_cell(atoms_rlx, strain_mask=[0, 0, 1, 0, 0, 0])

    del atoms_rlx.constraints

    e_ref = atoms_rlx.get_potential_energy()
    area = geometry.get_cell_area(atoms_rlx, [0, 1])

    # using ASE units to convert to mJ/m^2
    mult = eV / kJ * 1000 * m * m * 1000

    tilt_energy = []

    t = np.asarray(t_vec)

    delta = 1./npoints

    for n, d in enumerate(np.arange(0., 1. + delta, delta)):
        dtvec = d * t
        atoms_tilt = tilted_cell_sf.get_tilted_cell_dt(atoms_rlx.copy(), dtvec *
                                                       a)
        atoms_tilt.wrap()
        atoms_tilt = potential.set_calculator(atoms_tilt)

        atoms_tilt_rlx = relax_atoms_cell_direction(atoms_tilt,
                                                    strain_mask=[
                                                        0, 0, 1, 0, 0, 0],
                                                    atom_direction=[0, 0, 1])

        if(write):
            atoms_tilt_rlx.write("atoms_tilt_rlx_" +
                                 plane + "_" + str(n) + ".xyz")

        e_ref_tilt = atoms_tilt_rlx.get_potential_energy()
        tilt_energy.append([d, e_ref_tilt/area * mult])

    return tilt_energy


def get_energy_volume_curve(potential, element_index=0):
    """Function to calculate the energy-volume curve for hcp material"""

    from ase.build import bulk
    from ase.calculators.lammpslib import LAMMPSlib

    import numpy as np

    element = potential.get_elements()[0]
    a = potential.get_material_parameter('lattice constant')[0]
    covera = potential.get_material_parameter('c/a')[0]

    lattice_parameters = np.linspace(0.6 * a, 1.3 * a)

    value_pairs = []
    for lattice_parameter in lattice_parameters:
        atoms = bulk(element, 'hcp', a=lattice_parameter, covera=covera)
        atoms = potential.set_calculator(atoms)
        energy = atoms.get_potential_energy()
        volume = np.abs(np.linalg.det(atoms.cell))
        value_pairs.append([volume, energy])

    return value_pairs


def get_energy_a_curve(potential, element_index=0):
    """Function to calculate the energy-lattice constant curve for hcp material

    """

    from ase.build import bulk
    from ase.calculators.lammpslib import LAMMPSlib

    import numpy as np

    element = potential.get_elements()[0]
    a = potential.get_material_parameter('lattice constant')[0]
    covera = potential.get_material_parameter('c/a')[0]

    lattice_parameters = np.linspace(0.6 * a, 1.3 * a)

    value_pairs = []
    for lattice_parameter in lattice_parameters:
        atoms = bulk(element, 'hcp', a=lattice_parameter, covera=covera)
        atoms = potential.set_calculator(atoms)
        energy = atoms.get_potential_energy()
        value_pairs.append([lattice_parameter, energy])

    return value_pairs
