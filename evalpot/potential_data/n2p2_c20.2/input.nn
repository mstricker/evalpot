###############################################################################
# GENERAL NNP SETTINGS
###############################################################################
number_of_elements              1              # Number of elements.
elements                        Mg             # Specification of elements.
#atom_energy                     Mg  0.0         # Free atom reference energy (S).
cutoff_type                     2 0.0          # Cutoff type (optional argument: shift parameter alpha).
#scale_symmetry_functions                       # Scale all symmetry functions with min/max values.
#scale_symmetry_functions_sigma                 # Scale all symmetry functions with sigma.
scale_min_short                 0.0            # Minimum value for scaling.
scale_max_short                 1.0            # Maximum value for scaling.
#center_symmetry_functions                      # Center all symmetry functions, i.e. subtract mean value.
global_hidden_layers_short      2              # Number of hidden layers.
global_nodes_short              20 20          # Number of nodes in each hidden layer.
# was: global_activation_short         p p l          # Activation function for each hidden layer and output layer.
global_activation_short         t t l          # Giulio
#normalize_nodes                                # Normalize input of nodes.

###############################################################################
# ADDITIONAL SETTINGS FOR TRAINING
###############################################################################
epochs                          400             # Number of training epochs.
updater_type                    1              # Weight update method (0 = Gradient Descent, 1 = Kalman filter).
parallel_mode                   4              # Training parallelization used (0 = Serial, 1-4 = MSEKF implementations (2 or 4 are fastest)).
update_strategy                 0              # Update strategy (0 = Combined, 1 = Per-element).
selection_mode                  1              # Update candidate selection mode (0 = Random, 1 = Sort, 2 = Threshold).
memorize_symfunc_results                       # Keep symmetry function results in memory.
random_seed                     2091          # Random number generator seed.
test_fraction                   0.05           # Fraction of structures kept for testing.
use_short_forces                               # Use forces for training.
force_weight                    10.0           # Weight of force updates relative to energy updates.
short_energy_fraction           1.0           # Fraction of energy updates per epoch.
short_force_fraction            0.20        # Fraction of force updates per epoch.
short_energy_error_threshold    0.8           # RMSE threshold for energy update candidates.
short_force_error_threshold     0.8           # RMSE threshold for force update candidates.
rmse_threshold_trials           3              # Maximum number of RMSE threshold trials.
#repeated_energy_update                         # After force update perform energy update for corresponding structure.
#use_old_weights_short                          # Restart fitting with old weight parameters.
weights_min                     -1.0           # Minimum value for initial random weights.
weights_max                     1.0            # Maximum value for initial random weights.
#precondition_weights                           # Precondition weights with initial energies.
#nguyen_widrow_weights_short                    # Initialize neural network weights according to Nguyen-Widrow scheme.
write_trainpoints               10              # Write energy comparison every this many epochs.
write_trainforces               10              # Write force comparison every this many epochs.
write_weights_epoch             10              # Write weights every this many epochs.
write_neuronstats               10              # Write neuron statistics every this many epochs.
write_trainlog                  10              # Write training log file.
####################
# GRADIENT DESCENT #
####################
#gradient_type                   0              # Gradient descent type (0 = Fixed step size).
#gradient_eta                    1.0E-4         # Gradient descent parameter eta (fixed step size).
############################
# KALMAN FILTER (STANDARD) #
############################
kalman_type                     0              # Kalman filter type (0 = Standard, 1 = Fading memory).
kalman_epsilon                  1.0E-1         # General Kalman filter parameter epsilon (sigmoidal: 0.01, linear: 0.001).
kalman_q0                       0.00           # General Kalman filter parameter q0 ("large").
kalman_qtau                     2.302          # General Kalman filter parameter qtau (2.302 => 1 order of magnitude per epoch).
kalman_qmin                     1.0E-6         # General Kalman filter parameter qmin (typ. 1.0E-6).
kalman_eta                      0.9850           # Standard Kalman filter parameter eta (0.001-1.0).
kalman_etatau                   2.302          # Standard Kalman filter parameter etatau (2.302 => 1 order of magnitude per epoch).
kalman_etamax                   1.0            # Standard Kalman filter parameter etamax (1.0+).
#################################
# KALMAN FILTER (FADING MEMORY) #
#################################
# kalman_type                     1              # Kalman filter type (0 = Standard, 1 = Fading memory).
# kalman_epsilon                  1.0E-1         # General Kalman filter parameter epsilon (sigmoidal: 0.01, linear: 0.001).
# kalman_q0                       0.00           # General Kalman filter parameter q0 ("large").
# kalman_qtau                     2.302          # General Kalman filter parameter qtau (2.302 => 1 order of magnitude per epoch).
# kalman_qmin                     0.0E-6         # General Kalman filter parameter qmin (typ. 1.0E-6).
# kalman_lambda_short             0.9850        # Fading memory Kalman filter parameter lambda (forgetting factor 0.95-0.99).
# kalman_nue_short                0.99870        # Fading memory Kalman filter parameter nu (0.99-0.9995).

###############################################################################
# SYMMETRY FUNCTIONS
###############################################################################

# Radial symmetry function (type 2):
#symfunction_short <element-central> 2 <element-neighbor> <eta> <rshift> <rcutoff>

# Narrow Angular symmetry function (type 3):
#symfunction_short <element-central> 3 <element-neighbor1> <element-neighbor2> <eta> <lambda> <zeta> <rcutoff> <<rshift>

# Wide Angular symmetry function (type 9):
#symfunction_short <element-central> 9 <element-neighbor1> <element-neighbor2> <eta> <lambda> <zeta> <rcutoff> <<rshift>

# Markus symfuncs set
symfunction_short Mg 2 Mg 2.630E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 1.560E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 2.630E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 6.900E-03 0.000E+00 1.200E+01
symfunction_short Mg 2 Mg 4.420E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 1.560E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 7.430E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 7.430E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 4.420E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 1.560E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 1.117E-01 1.301E+01 1.600E+01
symfunction_short Mg 2 Mg 1.821E-01 5.657E+00 8.000E+00
symfunction_short Mg 2 Mg 4.420E-02 0.000E+00 8.000E+00
symfunction_short Mg 2 Mg 1.170E-02 0.000E+00 1.200E+01
symfunction_short Mg 2 Mg 1.081E-01 1.322E+01 2.000E+01
symfunction_short Mg 2 Mg 7.150E-02 1.626E+01 2.000E+01
symfunction_short Mg 2 Mg 9.110E-02 8.000E+00 1.600E+01
symfunction_short Mg 2 Mg 4.550E-02 1.131E+01 1.600E+01
symfunction_short Mg 2 Mg 1.166E-01 7.071E+00 2.000E+01
symfunction_short Mg 2 Mg 5.830E-02 1.000E+01 2.000E+01
symfunction_short Mg 2 Mg 2.910E-02 1.414E+01 2.000E+01
symfunction_short Mg 2 Mg 3.900E-03 0.000E+00 1.600E+01
symfunction_short Mg 2 Mg 2.500E-03 0.000E+00 2.000E+01
symfunction_short Mg 2 Mg 5.560E-02 0.000E+00 1.200E+01
symfunction_short Mg 2 Mg 8.700E-03 0.000E+00 2.000E+01
symfunction_short Mg 2 Mg 1.350E-02 0.000E+00 1.600E+01
symfunction_short Mg 3 Mg Mg 6.900E-03 1 1.0 1.200E+01
symfunction_short Mg 3 Mg Mg 6.900E-03 1 4.0 1.200E+01
symfunction_short Mg 3 Mg Mg 6.900E-03 -1 1.0 1.200E+01
symfunction_short Mg 3 Mg Mg 1.170E-02 1 1.0 1.200E+01
symfunction_short Mg 2 Mg 1.960E-02 0.000E+00 1.200E+01
symfunction_short Mg 3 Mg Mg 1.560E-02 1 1.0 8.000E+00